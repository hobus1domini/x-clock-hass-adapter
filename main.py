# !/usr/bin/python
# -*- coding: utf-8 -*-
import os
import threading

import paho.mqtt.client as mqtt
import json
import requests
from bs4 import BeautifulSoup

CLOCK_NO = os.environ.get('CLOCK_NO')
CLOCK_ADDR = os.environ.get('CLOCK_ADDR')
TIMEOUT = int(os.environ.get('TIMEOUT'))

CLOCK_ID = f"x-clock_{CLOCK_NO}"
TOPIC = f"x-clock/{CLOCK_NO}/#"
BROKER_ADDRESS = os.environ.get('BROKER_ADDRESS')
BROKER_PORT = int(os.environ.get("BROKER_PORT"))
BROKER_USERNAME = os.environ.get('BROKER_USERNAME')
BROKER_PASSWORD = os.environ.get('BROKER_PASSWORD')

main_topic = f"x-clock/{CLOCK_NO}/"


def create_light_object():
    light_object = {
        "name": f"X-Clock",
        "unique_id": CLOCK_ID,
        "icon": "mdi:clock",
        "brightness": True,
        "brightness_scale": 16,
        "color_mode": True,
        "supported_color_modes": ["brightness"],
        "command_topic": main_topic + "light",
        "state_topic": main_topic + "light/state",
        "brightness_command_topic": main_topic + "brightness",
        "brightness_state_topic": main_topic + "brightness/state",
        "availability_topic": main_topic + "state",
        "payload_available": "True",
        "payload_not_available": "False",
        "device": {
            "identifiers": [
                f"X-Clock {CLOCK_NO}"
            ],
            "manufacturer": "Pixelkoenig",
            "model": "x-clock",
            "name": f"X-Clock {CLOCK_NO}",
            "sw_version": extract_firmware(get_request(f"http://{CLOCK_ADDR}")),
            "hw_version": "ESP8266",
            "configuration_url": "https://o-clock.eu/",
            "suggested_area": "Living Room"
        },
        "qos": 0,
        "retain": True
    }
    client.publish(f"homeassistant/light/{CLOCK_ID}/light/config", json.dumps(light_object), qos=0, retain=True)

    ani_object = {
        "name": f"X-Clock 5min Animation",
        "unique_id": CLOCK_ID + "_ani",
        "icon": "mdi:animation-play",
        "command_topic": main_topic + "ani",
        "state_topic": main_topic + "ani/state",
        "availability_topic": main_topic + "state",
        "payload_available": "True",
        "payload_not_available": "False",
        "device": {
            "identifiers": [
                f"X-Clock {CLOCK_NO}"
            ]
        },
        "qos": 0,
        "retain": True
    }
    client.publish(f"homeassistant/switch/{CLOCK_ID}/ani/config", json.dumps(ani_object), qos=0, retain=True)


def get_request(url: str) -> str:
    response: bytes = b""
    session = requests.Session()

    try:
        # necessary to get input in stream to get all content before encoding error
        with session.get(url, stream=True, timeout=TIMEOUT, verify=False) as resp:
            for line in resp.iter_lines():
                if line:
                    response += line
    except requests.exceptions.ChunkedEncodingError:  # expected error because backend sends wrong char
        pass
    except requests.exceptions.ConnectTimeout:  # also not an error because device could be disconnected
        pass
    except Exception as e:
        print("Exception occurred:", e)
    finally:
        session.close()

    return response.decode("utf-8")


def extract_firmware(html: str) -> str:
    if html:
        soup = BeautifulSoup(html, 'html.parser')
        firmware = soup.find_all("p", class_="news")[1]
        return firmware.text
    else:
        return ""


def extract_state(html: str) -> str:
    if html:
        soup = BeautifulSoup(html, 'html.parser')
        state = soup.find_all("p", class_="news")[4]
        return state.text.upper()
    else:
        return "OFF"


def get_availability_state(address: str) -> bool:
    try:
        resp = requests.get(f"http://{address}", timeout=TIMEOUT, verify=False)
        if resp.status_code == 200:
            return True
    except requests.exceptions.ChunkedEncodingError:
        return True
    except requests.exceptions.ConnectTimeout:
        return False
    except Exception as e:
        print("Exception occurred:", e)

    return False


def regular_state_job():
    print("Running regular state job")
    create_light_object()

    url_ = f"http://{CLOCK_ADDR}"
    avail = get_availability_state(CLOCK_ADDR)
    client.publish(main_topic + "state", str(avail))

    if avail:
        r = get_request(url_)
        st = extract_state(r)
        client.publish(main_topic + "light/state", str(st))

    thread = threading.Timer(60, regular_state_job)
    thread.start()


def on_message(client, userdata, message):
    msg = str(message.payload.decode("utf-8"))
    tpc = message.topic
    tpc_short = tpc.split("/", 2)[2]

    print("topic: ", tpc_short)
    print("received: ", msg)

    if tpc_short == "light":
        print("x-clock state: " + str(msg))
        client.publish(main_topic + tpc_short + "/state", str(msg))
        if extract_state(get_request(f"http://{CLOCK_ADDR}")) != msg:
            get_request(f"http://{CLOCK_ADDR}/display?action=power{str(msg).lower()}")

    if tpc_short == "brightness":
        print("x-clock Brightness: " + str(int(msg) - 1))
        client.publish(main_topic + tpc_short + "/state", str(msg))
        get_request(f"http://{CLOCK_ADDR}/display?brightness={str(int(msg) - 1)}&action=savebrightness")

    if tpc_short == "ani":
        print("x-clock ani: " + str(msg))
        client.publish(main_topic + tpc_short + "/state", str(msg))
        get_request(
            f"http://{CLOCK_ADDR}/display?anim_5min={'in' if str(msg) == 'OFF' else ''}active&action=saveanim_5min")


def on_connect(client, userdata, flags, rc):
    print("Connected to MQTT Broker: " + BROKER_ADDRESS)
    client.subscribe(TOPIC)
    create_light_object()


if __name__ == "__main__":
    client = mqtt.Client(client_id="x-clock_hass_adapter")
    client.on_connect = on_connect
    client.on_message = on_message

    client.username_pw_set(BROKER_USERNAME, BROKER_PASSWORD)
    client.connect(BROKER_ADDRESS, BROKER_PORT, 60)

    regular_state_job()
    client.loop_forever()
