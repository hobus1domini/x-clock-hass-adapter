# X-Clock Hass Adapter

This is a hacky implementation for the Ikea Obegränsad hacked LED Panel by Pixelkoenig.

His software is available under https://o-clock.eu/.

Because there is no source code available,
I build this simple intermediate Container to give a simple API to Home Assistant via MQTT.

This is definitly no clean code, but it was created in an evening, so I don't care. Use at your own like and risk.

See `docker-compose.yml` for example usage. Login data should be provided via `.env` file which is excluded from 
version control system.
